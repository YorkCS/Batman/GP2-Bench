import gc
import os
from time import sleep
from sys import stdout, stderr
from grpc import server, Server
from traceback import print_exc
from concurrent.futures import ThreadPoolExecutor

from bench_pb2 import *
from bench_pb2_grpc import *

from gen import GenFactory
from gp2i import GP2IFactory
from bench import Bench, GP2Error


class Service(BenchServicer):

    def __init__(self, bench):
        self._bench = bench

    def Execute(self, request, context):
        progs = list(request.programs)

        if len(progs) == 0 or '' in list(map(lambda p: p.content, progs)):
            yield BenchStep(error='A non-empty set of non-empty programs must be provided.')
        elif request.runs < 1:
            yield BenchStep(error='The number of runs specified must be strictly positive.')
        else:
            try:
                for prog, done, err in self._execute(progs, request.config, request.runs):
                    if prog is not None:
                        yield BenchStep(progress=Progress(phase=prog[0], total=prog[1], complete=prog[2]))
                    if done is not None:
                        yield BenchStep(benchmarks=Benchmarks(program=done[0], results=done[1]))
                    elif err is not None:
                        yield BenchStep(error=err)
            except GeneratorExit:
                pass
            except GP2Error as e:
                yield BenchStep(error=str(e))
            except BaseException as e:
                print(e)
                print_exc()
                stdout.flush()
                stderr.flush()
                yield BenchStep(error='An error occured during benchmarking!')

        gc.collect()

    def _execute(self, progs, config, runs):
            graphs = []

            k = 0
            t = len(range(config.range.min, config.range.max + 1, config.range.step))
            yield (0, t, k), None, None
            for i, g in enumerate(self._bench.graphs(config)):
                graphs.append((i, g[0], g[1], g[2]))
                k += 1
                yield (0, t, k), None, None

            results = {}

            for j in range(len(progs)):
                results[j] = {}

            for g in graphs:
                for j in range(len(progs)):
                    results[j][g[0]] = {}

            k = 0
            t = runs*len(graphs)*len(progs)
            yield (1, t, k), None, None
            for j, index, i, result in self._bench.results(graphs, progs, runs):
                results[j][index][i] = result
                k += 1
                yield (1, t, k), None, None

            sizes = self._sizes(graphs)

            for j, r in results.items():
                yield None, (progs[j].name, list(self._average(r, sizes))), None


    def _average(self, results, sizes):
        for i in sorted(results):
            r = results[i]
            size = len(r)
            succ = sum(map(lambda x: int(x[0]), r.values()))
            time = sum(map(lambda x: x[1], r.values()))
            counts = sizes[i]
            yield Benchmark(nodes=counts[0], edges=counts[1], rate=float(succ)/size, time=float(time)/(size*1000))

    def _sizes(self, graphs):
        return {i:(n, e) for i,g,n,e in graphs}


class Factory(object):

    @staticmethod
    def make(service = None):
        MB = 1024 * 1024
        GRPC_CHANNEL_OPTIONS = [('grpc.max_message_length', 256 * MB), ('grpc.max_receive_message_length', 256 * MB)]

        s = server(ThreadPoolExecutor(max_workers=16), options=GRPC_CHANNEL_OPTIONS)
        add_BenchServicer_to_server(service or Factory._make_service(), s)
        s.add_insecure_port('[::]:9111')

        return s

    @staticmethod
    def _make_service(gp2c_path = None, gp2i_path = None):
        return Service(Factory._make_bench())

    @staticmethod
    def _make_bench(gen_host = None, gp2i_host = None):
        return Bench(
            GenFactory.make(os.environ['GEN_HOST']),
            GP2IFactory.make(os.environ['GP2I_HOST']),
            bool(int(os.environ.get('BENCH_STOP_ON_FAILURE', '0')))
        )


def main():
    s = Factory.make()
    s.start()

    try:
        while True:
            sleep(60 * 60 * 24)
    except KeyboardInterrupt:
        s.stop(0)


if __name__ == '__main__':
    main()
