from random import shuffle

from gp2i_pb2 import *
from gp2i_pb2_grpc import *

from gen_pb2 import *
from gen_pb2_grpc import *


class GP2Error(RuntimeError):
    pass


class Bench(object):

    def __init__(self, gen, gp2i, abort):
        self._gen = gen
        self._gp2i = gp2i
        self._abort = abort

    def graphs(self, config):
        for g in self._gen.handle(config):
            yield g

    def results(self, graphs, progs, runs):
        for i in range(runs):
            shuffle(graphs)
            for g in graphs:
                for j, p in enumerate(progs):
                    yield (j, g[0], i, self._time(p.content, g[1]))

    def _time(self, program, graph):
        graph, error, time = self._gp2i.handle(program, graph)

        if error is not None and self._abort:
            raise GP2Error(error)

        return error is None, time
