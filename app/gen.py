from grpc import insecure_channel

from gen_pb2 import *
from gen_pb2_grpc import *


class GenHandler(object):

    def __init__(self, client):
        self._client = client

    def handle(self, config):
        for response in self._client.Generate(config):
            yield response.graph, response.nodes, response.edges


class GenFactory(object):

    @staticmethod
    def make(host, port = None):
        MB = 1024 * 1024
        GRPC_CHANNEL_OPTIONS = [('grpc.max_message_length', 256 * MB), ('grpc.max_receive_message_length', 256 * MB)]

        return GenHandler(
            GenStub(insecure_channel('{}:{}'.format(host, port or 9111), options=GRPC_CHANNEL_OPTIONS))
        )
