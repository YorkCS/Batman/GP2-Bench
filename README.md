# GP2 Bench

## Prereqs

Any modern Linux or Mac running Docker 2018 CE or greater will suffice.

## Running

You can start the GP2 Bench Docker instance, by providing it with the hostname or IP address of a GP2I GRPC server and also a GP2 Gen server:

```bash
$ docker run -t -v ${PWD}:/data -e GP2I_HOST='172.17.0.2' -e GEN_HOST='172.17.0.3' registry.gitlab.com/yorkcs/batman/gp2-bench
```

You may configure GP2 Bench to quit immediately if there is an error with `-e BENCH_STOP_ON_FAILURE='1'`. By default, this is set to `0` so that failure cases can be timed too.

## Building

In standard Docker fashion:

```bash
$ docker build -t registry.gitlab.com/yorkcs/batman/gp2-bench:latest .
```

And then to publish:

```bash
$ docker push registry.gitlab.com/yorkcs/batman/gp2-bench
```
