FROM python:3.9-slim-buster
RUN pip install grpcio-tools
COPY proto /proto
WORKDIR /proto
RUN mkdir build
RUN python -m grpc_tools.protoc -I. --python_out=build --grpc_python_out=build *.proto

FROM python:3.9-slim-buster
RUN pip install grpcio protobuf
COPY app /app
COPY --from=0 /proto/build /app
RUN mkdir /data
WORKDIR /app
EXPOSE 9111
ENTRYPOINT ["python", "main.py"]
